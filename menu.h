#ifndef MENU_H
#define MENU_H

#pragma once
#include "SFML/Graphics.hpp"
#include "button.h"
#define MAX_NUMBER_OF_ITEMS 3

class Menu
{
public:
    Menu(float width, float height);
    ~Menu();

    void draw(sf::RenderWindow &window);
    void MoveLeft();
    void MoveRight();
    int getPressedItem() { return selectedItemIndex; }
    Button getButton(int x);

private:
    int selectedItemIndex;
    sf::Font font;
    Button menu[MAX_NUMBER_OF_ITEMS];
};

#endif // MENU_H
