#ifndef BLAST_H
#define BLAST_H

#include <ostream>

#include "carte.h"

class Blast : public Carte
{
public:
    Blast();
    void getEffect(Player *p1, Player *p2) override;
    Blast(int x, int y);

private:
    int manaCost = 1;
    int sDmg = 1;
};

//std::ostream& operator<< (std::ostream &out, const Player &p);

#endif // BLAST_H
