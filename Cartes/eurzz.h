#ifndef EURZZ_H
#define EURZZ_H

#include <ostream>

#include "carte.h"

class Eurzz : public Carte
{
public:
    Eurzz();
    void getEffect(Player *p1, Player *p2) override;
    Eurzz(int x, int y);

private:
    int manaCost = 4;
    int sArmor = 2;
    int sDmg = 1;
};

#endif // EURZZ_H
