#include <iostream>

#include "pyrou.h"

Pyrou::Pyrou()
{

}

void Pyrou::getEffect(Player *p1, Player *p2)
{
    if(p1->mana->getValue() >= manaCost)
    {
        p1->mana->UpdateValue(manaCost * -1);
        if(p2->armor->getValue() == 0)
        {
            p2->health->UpdateValue(sDmg * -1);
        }
        else
        {
            p2->armor->UpdateValue(sDmg * -1);
        }
    }
}

Pyrou::Pyrou(int x, int y):
    Carte("Pyrou", 5, Fire, "./assets/Pyrou.png", x, y)
{

}
