#ifndef EURSS_H
#define EURSS_H

#include <ostream>

#include "carte.h"

class Eurss : public Carte
{
public:
    Eurss();
    void getEffect(Player *p1, Player *p2) override;
    Eurss(int x, int y);

private:
    int manaCost = 2;
    int sArmor = 1;
    int sDmg = 1;
};

#endif // EURSS_H
