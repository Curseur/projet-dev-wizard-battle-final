#ifndef FROST_H
#define FROST_H

#include <ostream>

#include "carte.h"

class Frost : public Carte
{
public:
    Frost();
    void getEffect(Player *p1, Player *p2) override;
    Frost(int x, int y);

private:
    int manaCost = 4;
    int sGel = 2;
    int sDmg = 2;
};

#endif // FROST_H
