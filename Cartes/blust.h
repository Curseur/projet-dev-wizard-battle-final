#ifndef BLUST_H
#define BLUST_H

#include <ostream>

#include "carte.h"

class Blust : public Carte
{
public:
    Blust();
    void getEffect(Player *p1, Player *p2) override;
    Blust(int x, int y);

private:
    int manaCost = 2;
    int sDmg = 2;
};

#endif // BLUST_H
