#include <iostream>

#include "frost.h"

Frost::Frost()
{

}

void Frost::getEffect(Player *p1, Player *p2)
{
    if(p1->mana->getValue() >= manaCost)
    {
        p1->mana->UpdateValue(manaCost * -1);
        if(p2->mana->getValue() == 0 && p2->armor->getValue() == 0)
        {
            p2->mana->UpdateValue(sGel * -1);
            p2->health->UpdateValue(sDmg * -1);
        }
        else
        {
            p2->mana->UpdateValue(sGel * -1);
            p2->armor->UpdateValue(sDmg * -1);
        }
    }
}

Frost::Frost(int x, int y):
    Carte("Frost", 4, Ice, "./assets/Frost.png", x, y)
{

}
