#include <iostream>

#include "eurzz.h"

Eurzz::Eurzz()
{

}

void Eurzz::getEffect(Player *p1, Player *p2)
{
    if(p1->mana->getValue() >= manaCost)
    {
        p1->mana->UpdateValue(manaCost * -1);
        if(p1->armor->getValue() == 0 && p2->armor->getValue() == 0)
        {
            p1->armor->UpdateValue(sArmor * 1);
            p2->health->UpdateValue(sDmg * -1);
        }
        else
        {
            p1->armor->UpdateValue(sArmor * 1);
            p2->armor->UpdateValue(sDmg * -1);
        }
    }
}

Eurzz::Eurzz(int x, int y):
    Carte("Eurzz", 4, Earth, "./assets/Eurzz.png", x, y)
{

}
