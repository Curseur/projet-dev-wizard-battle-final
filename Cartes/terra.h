#ifndef TERRA_H
#define TERRA_H

#include <ostream>

#include "carte.h"

class Terra : public Carte
{
public:
    Terra();
    void getEffect(Player *p1, Player *p2) override;
    Terra(int x, int y);

private:
    int manaCost = 5;
    int sArmor = 3;
    int sDmg = 2;
};

#endif // TERRA_H
