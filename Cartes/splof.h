#ifndef SPLOF_H
#define SPLOF_H

#include <ostream>

#include "carte.h"

class Splof : public Carte
{
public:
    Splof();
    void getEffect(Player *p1, Player *p2) override;
    Splof(int x, int y);

private:
    int manaCost = 3;
    int sDmg = 3;
};

#endif // SPLOF_H
