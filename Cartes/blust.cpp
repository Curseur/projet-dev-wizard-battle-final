#include <iostream>

#include "blust.h"

Blust::Blust()
{

}

void Blust::getEffect(Player *p1, Player *p2)
{
    if(p1->mana->getValue() >= manaCost)
    {
        p1->mana->UpdateValue(manaCost * -1);
        if(p2->armor->getValue() == 0)
        {
            p2->health->UpdateValue(sDmg * -1);
        }
        else
        {
            p2->armor->UpdateValue(sDmg * -1);
        }
    }
}

Blust::Blust(int x, int y):
    Carte("Blust", 2, Fire, "./assets/Blust.png", x, y)
{

}
