#ifndef EARTT_H
#define EARTT_H

#include <ostream>

#include "carte.h"

class Eartt : public Carte
{
public:
    Eartt();
    void getEffect(Player *p1, Player *p2) override;
    Eartt(int x, int y);

private:
    int manaCost = 1;
    int sArmor = 1;
};

#endif // EARTT_H
