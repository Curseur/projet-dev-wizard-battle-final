#include <iostream>

#include "eurss.h"

Eurss::Eurss()
{

}

void Eurss::getEffect(Player *p1, Player *p2)
{
    if(p1->mana->getValue() >= manaCost)
    {
        p1->mana->UpdateValue(manaCost * -1);
        if(p1->armor->getValue() == 0 && p2->armor->getValue() == 0)
        {
            p1->armor->UpdateValue(sArmor * 1);
            p2->health->UpdateValue(sDmg * -1);
        }
        else
        {
            p1->armor->UpdateValue(sArmor * 1);
            p2->armor->UpdateValue(sDmg * -1);
        }
    }
}

Eurss::Eurss(int x, int y):
    Carte("Eurss", 2, Earth, "./assets/Eurss.png", x, y)
{

}
