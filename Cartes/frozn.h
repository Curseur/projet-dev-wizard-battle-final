#ifndef FROZN_H
#define FROZN_H

#include <ostream>

#include "carte.h"

class Frozn : public Carte
{
public:
    Frozn();
    void getEffect(Player *p1, Player *p2) override;
    Frozn(int x, int y);

private:
    int manaCost = 2;
    int sGel = 1;
    int sDmg = 1;
};

#endif // FROZN_H
