#ifndef BOLTZ_H
#define BOLTZ_H

#include <ostream>

#include "carte.h"

class Boltz : public Carte
{
public:
    Boltz();
    void getEffect(Player *p1, Player *p2) override;
    Boltz(int x, int y);

private:
    int manaCost = 5;
    int sGel = 3;
    int sDmg = 2;
};

#endif // BOLTZ_H
