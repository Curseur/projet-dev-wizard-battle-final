#ifndef PYROU_H
#define PYROU_H

#include <ostream>

#include "carte.h"

class Pyrou : public Carte
{
public:
    Pyrou();
    void getEffect(Player *p1, Player *p2) override;
    Pyrou(int x, int y);

private:
    int manaCost = 5;
    int sDmg = 5;
};

#endif // PYROU_H
