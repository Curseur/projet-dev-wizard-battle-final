#ifndef FREZI_H
#define FREZI_H

#include <ostream>

#include "carte.h"

class Frezi : public Carte
{
public:
    Frezi();
    void getEffect(Player *p1, Player *p2) override;
    Frezi(int x, int y);

private:
    int manaCost = 1;
    int sGel = 1;
};

#endif // FREZI_H
