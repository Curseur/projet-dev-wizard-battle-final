#define LOG(msg) \
    std::cout << __FILE__ << "(" << __LINE__ << "): " << msg << std::endl

#include <iostream>

#include "blast.h"

Blast::Blast()
{

}

void Blast::getEffect(Player *p1, Player *p2)
{
    if(p1->mana->getValue() >= manaCost)
    {
        p1->mana->UpdateValue(manaCost * -1);
        if(p2->armor->getValue() == 0)
        {
            p2->health->UpdateValue(sDmg * -1);
        }
        else
        {
            p2->armor->UpdateValue(sDmg * -1);
        }
    }
}

Blast::Blast(int x, int y):
    Carte("Blast", 1, Fire, "./assets/Blast.png", x, y)
{

}

//std::ostream& operator<<(std::ostream &out, const Player &p)
//{
//    out << "Health : " << p.health->getValue() << ", ";
//    out << "Armor : " << p.armor->getValue() << ", ";
//    out << "Mana : " << p.mana->getValue();
//    return out;
//}
