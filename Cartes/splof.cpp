#include <iostream>

#include "splof.h"

Splof::Splof()
{

}

void Splof::getEffect(Player *p1, Player *p2)
{
    if(p1->mana->getValue() >= manaCost)
    {
        p1->mana->UpdateValue(manaCost * -1);
        if(p2->armor->getValue() == 0)
        {
            p2->health->UpdateValue(sDmg * -1);
        }
        else
        {
            p2->armor->UpdateValue(sDmg * -1);
        }
    }
}

Splof::Splof(int x, int y):
    Carte("Splof", 3, Fire, "./assets/Splof.png", x, y)
{

}
