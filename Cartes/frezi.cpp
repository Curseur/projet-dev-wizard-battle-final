#include <iostream>

#include "frezi.h"

Frezi::Frezi()
{

}

void Frezi::getEffect(Player *p1, Player *p2)
{
    if(p1->mana->getValue() >= manaCost)
    {
        p1->mana->UpdateValue(manaCost * -1);
        if(p2->mana->getValue() == 0)
        {
            p2->mana->UpdateValue(sGel * -1);
        }
        else
        {
            p2->mana->UpdateValue(sGel * -1);
        }
    }
}

Frezi::Frezi(int x, int y):
    Carte("Frezi", 1, Ice, "./assets/Frezi.png", x, y)
{

}
