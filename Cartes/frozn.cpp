#include <iostream>

#include "frozn.h"

Frozn::Frozn()
{

}

void Frozn::getEffect(Player *p1, Player *p2)
{
    if(p1->mana->getValue() >= manaCost)
    {
        p1->mana->UpdateValue(manaCost * -1);
        if(p2->mana->getValue() == 0 && p2->armor->getValue() == 0)
        {
            p2->mana->UpdateValue(sGel * -1);
            p2->health->UpdateValue(sDmg * -1);
        }
        else
        {
            p2->mana->UpdateValue(sGel * -1);
            p2->armor->UpdateValue(sDmg * -1);
        }
    }
}

Frozn::Frozn(int x, int y):
    Carte("Frozn", 2, Ice, "./assets/Frozn.png", x, y)
{

}
