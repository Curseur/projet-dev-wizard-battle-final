#include <iostream>

#include "player.h"

Player::Player(std::string name,
               const std::string texture,
               sf::Vector2<int> pos,
               sf::Vector2<int> size)
{
    _name = name;
    _position.x = pos.x;
    _position.y = pos.y;
    _size.x = size.x;
    _size.y = size.y;

//    LOG(texture);
//    LOG(pos.x << " " << pos.y);
//    LOG(_position.x << " " << _position.y);

    if (!_image.loadFromFile(texture))
    {
        std::cout << "miss" << std::endl;
    }
    _sprite.setTexture(_image);
    _sprite.setTextureRect(sf::IntRect(0, 0, _size.x, _size.y));
    _sprite.setPosition(pos.x, pos.y);
}

sf::Sprite Player::getSprite()
{
//    std::cout << "Test position : " << _sprite.getPosition().x << std::endl;
//    std::cout << "Test position : " << _sprite.getPosition().y << std::endl;
//    std::cout << "Test size : " << _size.x << std::endl;
//    std::cout << "Test size : " << _size.y << std::endl;
    return _sprite;
}

void Player::setPos(sf::Vector2<int> p)
{
    _position = p;
}

Player::~Player()
{
    delete health;
    delete armor;
    delete mana;
}
