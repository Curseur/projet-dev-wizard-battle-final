#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "stats.h"

class Player
{
protected:
    std::string _name;
    sf::Texture _image;
    sf::Sprite _sprite;
    //Postion et taille de mon joueur
    sf::Vector2<int> _position;
    sf::Vector2<int> _size;

public:
    Player(std::string name,
           const std::string texture,
           sf::Vector2<int> pos = {0, 319},
           sf::Vector2<int> size = {218, 319});
    Stats* health;
    Stats* armor;
    Stats* mana;
    sf::Sprite getSprite();
    virtual void getStatus() = 0;
    void setPos(sf::Vector2<int> p);

    ~Player();
};

#endif // PLAYER_H
