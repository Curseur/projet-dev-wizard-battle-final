#define LOG(msg) \
    std::cout << __FILE__ << "(" << __LINE__ << "): " << msg << std::endl

#include <iostream>

#include <experimental/filesystem>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include "gameplay.h"

#include "PlayersTypes/wizardgood.h"
#include "PlayersTypes/misterdark.h"

#include "./Cartes/blast.h"
#include "./Cartes/blust.h"
#include "./Cartes/splof.h"
#include "./Cartes/pyrou.h"

#include "./Cartes/frezi.h"
#include "./Cartes/frozn.h"
#include "./Cartes/frost.h"
#include "./Cartes/boltz.h"

#include "./Cartes/eartt.h"
#include "./Cartes/eurss.h"
#include "./Cartes/eurzz.h"
#include "./Cartes/terra.h"

template <class TShape>
bool shape_contains_cursor(TShape& shape, sf::Vector2f const& cursor_position) {
  return shape.getGlobalBounds().contains(cursor_position.x, cursor_position.y);
}

sf::RenderWindow renderGame;
sf::Texture textureGame;
sf::Sprite spriteGame;

Gameplay::Gameplay()
{

}

Gameplay::Gameplay(sf::RenderWindow* renderWindow)
{


    textureGame.loadFromFile("./assets/PixelArt.jpg");
    spriteGame.setTexture(textureGame);
    renderGame.setFramerateLimit(60);

    std::cout << std::experimental::filesystem::current_path() << "./assets/PixelArt.jpg" << std::endl;
    std::cout << "Lancement de la partie" << std::endl;

    while (renderWindow->isOpen())
    {
        // Process events
            sf::Event event;
            while (renderWindow->pollEvent(event))
            {
                // Close window: exit
                if (event.type == sf::Event::Closed)
                    renderWindow->close();
            }

            Wizardgood *p1 = new Wizardgood; // health 10, armor 5, mana 0
            Misterdark *p2 = new Misterdark; // health 10, armor 5, mana 0
            Boltz card1;
            card1.getEffect(p1, p2);
//            std::cout << "Player 1 : " << p1 << std::endl; // health 10, armor 2, mana 0
//            std::cout << "Player 2 : " << p2 << std::endl; // health 10, armor 5, mana 0


//            Blast blast(0, 0);
//            Blust blust(0, 180);
//            Splof splof(0, 360);
//            Pyrou pyrou(0, 540);

//            Frezi frezi(0, 0);
//            Frozn frozn(0, 180);
//            Frost frost(0, 360);
//            Boltz boltz(0, 540);

//            Eartt eartt(0, 0);
//            Eurss eurss(0, 180);
//            Eurzz eurzz(0, 360);
//            Terra terra(0, 540);

            renderWindow->clear();
            renderWindow->draw(spriteGame);
//            ------------------------------------------
            renderWindow->draw(p1->getSprite());
            renderWindow->draw(p1->health->getSprite());
            renderWindow->draw(p1->armor->getSprite());
            renderWindow->draw(p1->mana->getSprite());
//            ------------------------------------------
            renderWindow->draw(p2->getSprite());
            renderWindow->draw(p2->health->getSprite());
            renderWindow->draw(p2->armor->getSprite());
            renderWindow->draw(p2->mana->getSprite());

//            Spell feu
//            renderWindow->draw(blast.getSprite());
//            renderWindow->draw(blust.getSprite());
//            renderWindow->draw(splof.getSprite());
//            renderWindow->draw(pyrou.getSprite());

//            Spell glace
//            renderWindow->draw(frezi.getSprite());
//            renderWindow->draw(frozn.getSprite());
//            renderWindow->draw(frost.getSprite());
//            renderWindow->draw(boltz.getSprite());

//            Spell terre
//            renderWindow->draw(eartt.getSprite());
//            renderWindow->draw(eurss.getSprite());
//            renderWindow->draw(eurzz.getSprite());
//            renderWindow->draw(terra.getSprite());

            renderWindow->display();

            if(p1->health ->getValue() == 0)
            {
                renderWindow->close();
            }
            if(p2->health ->getValue() == 0)
            {
                renderWindow->close();
            }

    }
}
