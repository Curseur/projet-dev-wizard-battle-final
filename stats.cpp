#define LOG(msg) \
    std::cout << __FILE__ << "(" << __LINE__ << "): " << msg << std::endl

#include <regex>
#include <iostream>

#include "stats.h"

Stats::Stats(int val,
             int min,
             int max,
             const std::string tex,
             sf::Vector2<int> pos,
             sf::Vector2<int> sprSize)
{
    mValue = val;
    mMin = min;
    mMax = max;
    mPos = pos;
    FileName = tex;

    mTexture.loadFromFile(FileName);
    mSpriteSize = sprSize;

    mSprite.setTexture(mTexture);
    UpdateSprite();
}

void Stats::UpdateValue(int newVal)
{
    if (mValue + newVal > mMax) mValue = mMax;
    else if (mValue + newVal < mMin) mValue = mMin;
    else mValue += newVal;

    UpdateSprite();
}

int Stats::getValue()
{
    return mValue;
}

sf::Sprite Stats::getSprite()
{
    return mSprite;
}

void Stats::UpdateSprite()
{
    mSprite.setTextureRect(sf::IntRect(0, mValue * mSpriteSize.y, mSpriteSize.x, mSpriteSize.y));
    mSprite.setPosition(mPos.x, mPos.y);
}
