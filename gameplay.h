#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include <SFML/Graphics/RenderWindow.hpp>

class Gameplay
{
public:
    Gameplay();
    Gameplay(sf::RenderWindow*);
};

#endif // GAMEPLAY_H
