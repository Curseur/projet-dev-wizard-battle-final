#include <iostream>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "button.h"

Button::Button()
{

}

Button::Button(int x, int y, int i, int j, const std::string &textureButton)
{
    _position.x = x;
    _position.y = y;
    _size.x = i;
    _size.y = j;
    if (!_texture.loadFromFile(textureButton, sf::IntRect(0, 0, i, j)))
    {
        std::cout << "mince" << std::endl;
    }
}

sf::Vector2<int> Button::getPosition()
{
    return _position;
}

sf::Vector2<int> Button::getSize()
{
    return _size;
}

sf::Texture Button::getTexture()
{
    return _texture;
}

void Button::setPosition(float x, float y)
{
    _position.x = x;
    _position.y = y;
}

void Button::setSize(int x, int y)
{
    _size.x = x;
    _size.y = y;
}

void Button::setTexture(const std::string &textureButton, int x, int y)
{
    if (!_texture.loadFromFile(textureButton, sf::IntRect(0, 0, x, y)))
    {
        std::cout << "mince" << std::endl;
    }
}

void Button::createSprite(int x)
{
    _sprite.setTexture(_texture);
    _sprite.setTextureRect(sf::IntRect(_size.x * x, 0, _size.x, _size.y));
    _sprite.setPosition(_position.x, _position.y);
}

sf::Sprite Button::getSprite()
{
    return _sprite;
}
