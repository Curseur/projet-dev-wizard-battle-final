#ifndef CARTE_H
#define CARTE_H

#include <string>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "PlayersTypes/wizardgood.h"
#include "PlayersTypes/misterdark.h"

enum Element{Fire, Ice, Earth};

class Carte
{
public:
    Carte();
    Carte(std::string name, int ressource, Element element, const std::string &textureCard, int x, int y, int i = 150, int j = 182);
    sf::Sprite getSprite();
    virtual void getEffect(Player* p1, Player* p2) = 0;

private:
    std::string _name;
    int _ressource;
    Element _element;
    sf::Texture _image;
    sf::Sprite _sprite;
    sf::Vector2<int> _position;
    sf::Vector2<int> _size;
};

#endif // CARTE_H
