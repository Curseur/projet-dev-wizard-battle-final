#ifndef BLAST_H
#define BLAST_H

#include "carte.h"

class Blast : public Carte
{
public:
    Blast();
    void getEffect() override;
    Blast(int x, int y);
};

#endif // BLAST_H
