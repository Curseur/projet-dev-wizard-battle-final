#include <iostream>
#include <string>
#include <cstdlib>
#include <experimental/filesystem>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "menu.h"
#include "gameplay.h"

using namespace std;

template <class TShape>
bool shape_contains_cursor(TShape& shape, sf::Vector2f const& cursor_position) {
  return shape.getGlobalBounds().contains(cursor_position.x, cursor_position.y);
}

//Variables
sf::RenderWindow renderWindow;
sf::Texture textureWindow;
sf::Sprite spriteWindow;

sf::Sprite spriteButton;

sf::Clock inputClock;
sf::Event event;

int main()
{
        //Création de la fêtetre Menu 1280x720
        renderWindow.create(sf::VideoMode(1280,720), "Battle Wizard");

        textureWindow.loadFromFile("./assets/MenuFinal.jpg");
        spriteWindow.setTexture(textureWindow);
        renderWindow.setFramerateLimit(60);

        //std::cout << std::experimental::filesystem::current_path() << "./assets/Menu.jpg" << std::endl;
        std::cout << "Lancement du jeu" << std::endl;



        Menu menu(renderWindow.getSize().x, renderWindow.getSize().y);

        //Tant que la fenêtre est ouverte
        while (renderWindow.isOpen())
        {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && inputClock.getElapsedTime().asMilliseconds() >= 75)
            {
                menu.MoveLeft();
                std::cout << "Left" << std::endl;
                inputClock.restart();
            }
            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && inputClock.getElapsedTime().asMilliseconds() >= 75)
            {
                menu.MoveRight();
                std::cout << "Right" << std::endl;
                inputClock.restart();
            }
            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Enter) && inputClock.getElapsedTime().asMilliseconds() >= 75)
            {
                switch (menu.getPressedItem())
                {
                    case 0:
                        std::cout << "Options button pressed" << std::endl;
                    break;
                    case 1:
                        //std::cout << "Play button pressed" << std::endl;
                        {Gameplay gameStart(&renderWindow);}
                    break;
                    case 2:
                        renderWindow.close();
                    break;
                }
            }

            // Process events
                sf::Event event;
                while (renderWindow.pollEvent(event))
                {
                    // Close window: exit
                    if (event.type == sf::Event::Closed)
                        renderWindow.close();
                    else if (event.type == sf::Event::MouseButtonPressed)
                    {
                        if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && inputClock.getElapsedTime().asMilliseconds() >= 75)
                        {
                            if (event.mouseButton.x >= menu.getButton(1).getPosition().x && event.mouseButton.y >= menu.getButton(1).getPosition().y && event.mouseButton.x < menu.getButton(1).getPosition().x + menu.getButton(1).getSize().x && event.mouseButton.y < menu.getButton(1).getPosition().y + menu.getButton(1).getSize().y)
                            {
                                Gameplay gameStart(&renderWindow);
                            }
                            else if (event.mouseButton.x >= menu.getButton(0).getPosition().x && event.mouseButton.y >= menu.getButton(0).getPosition().y && event.mouseButton.x < menu.getButton(0).getPosition().x + menu.getButton(0).getSize().x && event.mouseButton.y < menu.getButton(0).getPosition().y + menu.getButton(0).getSize().y)
                            {
                                //Gameplay gameStart(&renderWindow);
                                //Options optionsGame(&renderWindow);
                            }
                            else if (event.mouseButton.x >= menu.getButton(2).getPosition().x && event.mouseButton.y >= menu.getButton(2).getPosition().y && event.mouseButton.x < menu.getButton(2).getPosition().x + menu.getButton(2).getSize().x && event.mouseButton.y < menu.getButton(2).getPosition().y + menu.getButton(2).getSize().y)
                            {
                                renderWindow.close();
                            }
                        }
                    }
                }

                renderWindow.clear();
                renderWindow.draw(spriteWindow);
                menu.draw(renderWindow);
                renderWindow.display();
        }

        return 0;
}
