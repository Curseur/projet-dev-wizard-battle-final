#include <iostream>

#include "carte.h"

Carte::Carte()
{

}

Carte::Carte(std::string name, int ressource, Element element, const std::string &textureCard, int x, int y, int i, int j)
{
    _name = name;
    _ressource = ressource;
    _element = element;
    _position.x = x;
    _position.y = y;
    _size.x = i;
    _size.y = j;
    if (!_image.loadFromFile(textureCard, sf::IntRect(0, 0, i, j)))
    {
        std::cout << "mince" << std::endl;
    }
    _sprite.setTexture(_image);
    _sprite.setTextureRect(sf::IntRect(_size.x * x, 0, _size.x, _size.y));
    _sprite.setPosition(_position.x, _position.y);
}

sf::Sprite Carte::getSprite()
{
    return _sprite;
}
