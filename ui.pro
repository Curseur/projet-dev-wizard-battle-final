
TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#WINDOWS
# put SFML file in the same location as project
LIBS += -L"..\SFML-2.5.1\lib" #change this
LIBS += -lstdc++fs #change this
LIBS += -L"..\SFML-2.5.1\bin" #change this

#MAC
#LIBS += -L"/usr/local/lib"

CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-system -lsfml-network -lsfml-window
CONFIG(debug  , debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-system -lsfml-network -lsfml-window

#WINDOWS
INCLUDEPATH += "..\SFML-2.5.1\include" #change this
DEPENDPATH += "..\SFML-2.5.1\include" #change this

SOURCES += \
    Cartes\blast.cpp \
    Cartes\blust.cpp \
    Cartes\boltz.cpp \
    button.cpp \
    carte.cpp \
    deck.cpp \
    Cartes\eartt.cpp \
    Cartes\eurss.cpp \
    Cartes\eurzz.cpp \
    Cartes\frezi.cpp \
    Cartes\frost.cpp \
    Cartes\frozn.cpp \
    gameplay.cpp \
    hand.cpp \
    stats.cpp \
    main.cpp \   
    menu.cpp \
    PlayersTypes/misterdark.cpp \
    Cartes\pyrou.cpp \
    Cartes\splof.cpp \
    Cartes\terra.cpp \
    player.cpp \
    PlayersTypes/wizardgood.cpp

RESOURCES += \
    ressources.qrc

FORMS +=

HEADERS += \
    Cartes\blast.h \
    Cartes\blust.h \
    Cartes\boltz.h \
    button.h \
    carte.h \
    deck.h \
    Cartes\eartt.h \
    Cartes\eurss.h \
    Cartes\eurzz.h \
    Cartes\frezi.h \
    Cartes\frost.h \
    Cartes\frozn.h \
    gameplay.h \
    hand.h \
    menu.h \
    PlayersTypes/misterdark.h \
    Cartes\pyrou.h \
    Cartes\splof.h \
    Cartes\terra.h \
    player.h \
    PlayersTypes/wizardgood.h \
    stats.h

DISTFILES += \
    assets/Earth.png
