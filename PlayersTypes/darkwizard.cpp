#include <iostream>

#include "darkwizard.h"

Darkwizard::Darkwizard(std::string name, int health, int armor, int mana, const std::string &textureDarkWizard, int x, int y, int i, int j)
{
    _name = name;
    _health = health;
    _armor = armor;
    _mana = mana;
    _positionDarkWizard.x = x;
    _positionDarkWizard.y = y;
    _sizeDarkWizard.x = i;
    _sizeDarkWizard.y = j;
    if (!_imageDarkWizard.loadFromFile(textureDarkWizard, sf::IntRect(0, 0, i, j)))
    {
        std::cout << "miss" << std::endl;
    }
    _spriteDarkWizard.setTexture(_imageDarkWizard);
    _spriteDarkWizard.setTextureRect(sf::IntRect(_sizeDarkWizard.x * x, 0, _sizeDarkWizard.x, _sizeDarkWizard.y));
    _spriteDarkWizard.setPosition(_positionDarkWizard.x, _positionDarkWizard.y);
}

sf::Sprite Darkwizard::getSpriteDarkWizard()
{
    return _spriteDarkWizard;
}

//sf::Vector2<int> Darkwizard::getHealth()
//{
//    return _health;
//}

//sf::Vector2<int> Darkwizard::getArmor()
//{
//    return _armor;
//}

//sf::Vector2<int> Darkwizard::getMana()
//{
//    return _mana;
//}

//sf::Texture Darkwizard::getTextureDarkWizard()
//{
//    return _imageWizard;
//}
