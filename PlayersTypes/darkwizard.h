#ifndef DARKWIZARD_H
#define DARKWIZARD_H

#include <string>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "player.h"

class Darkwizard : public Player
{
public:
    Darkwizard();
    Darkwizard(std::string name, int health, int armor, int mana, const std::string &textureWizard, int x, int y, int i = 300, int j = 350);
    sf::Sprite getSpriteDarkWizard();
    virtual void getStatusDarkWizard() = 0;
//    sf::Vector2<int> getHealth();
//    sf::Vector2<int> getArmor();
//    sf::Vector2<int> getMana();
//    sf::Texture getTextureDarkWizard();
//    void setHealth(int stats);
//    void setArmor(int stats);
//    void setMana(int stats);
//    void setTextureDarkWizard(const std::string &textureDarkWizard, int x, int y);

private:
    std::string _name;
    int _health;
    int _armor;
    int _mana;
//    sf::Vector2<int> _health;
//    sf::Vector2<int> _armor;
//    sf::Vector2<int> _mana;
    sf::Texture _imageDarkWizard;
    sf::Sprite _spriteDarkWizard;
    sf::Vector2<int> _positionDarkWizard;
    sf::Vector2<int> _sizeDarkWizard;
};

#endif // DARKWIZARD_H
