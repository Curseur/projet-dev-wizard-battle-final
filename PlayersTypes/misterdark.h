#ifndef MISTERDARK_H
#define MISTERDARK_H

#include "player.h"

class Misterdark : public Player
{
public:
    Misterdark(int x = 1100, int y = 360);
    void getStatus() override;
};

#endif // MISTERDARK_H
