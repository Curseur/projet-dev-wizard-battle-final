#include "misterdark.h"

Misterdark::Misterdark(int x, int y):
    Player("MisterDark", "./assets/Wizard2.png", {x, y})
{
    health = new Stats(10, 0, 10, "./assets/hearts.png", {960, 0}, {308, 60});
    armor = new Stats(5, 0, 10, "./assets/armors.png", {1035, 70}, {232, static_cast<int>(59.9)});
    mana = new Stats(5, 0, 5, "./assets/mana.png", {1000, 140}, {267, 57});
}

void Misterdark::getStatus()
{

}
