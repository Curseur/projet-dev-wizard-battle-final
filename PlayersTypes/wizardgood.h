#ifndef WIZARDGOOD_H
#define WIZARDGOOD_H

#include "player.h"

class Wizardgood : public Player
{
public:
    Wizardgood(int x = 10, int y = 370);
    void getStatus() override;
};

#endif // WIZARDGOOD_H
