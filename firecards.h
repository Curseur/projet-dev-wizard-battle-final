#ifndef FIRECARDS_H
#define FIRECARDS_H

#pragma once
#include <algorithm>
#include <string>
#include <ctype.h>
#include <locale>

using namespace std;

class FireCards/* : public Cartes*/
{
public:
    FireCards();
    FireCards(std::string element, std::string spell);
    enum Element {Fire};
    enum Spell {feu, feuu, Feu, Feuu, feu2, feuu2, Feu2, Feuu2};
    FireCards(Element, Spell);
    std::string getElement();
    std::string getSpell();
    FireCards* next = NULL;

private:
    FireCards::Element _element;
    FireCards::Spell _spell;
    void setElement(std::string element);
    void setSpell(std::string spell);
};

#endif // FIRECARDS_H
