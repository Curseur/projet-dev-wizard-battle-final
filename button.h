#ifndef BUTTON_H
#define BUTTON_H

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

class Button
{
public:
    Button();
    Button(int x, int y, int i, int j, const std::string &textureButton);
    sf::Vector2<int> getPosition();
    sf::Vector2<int> getSize();
    sf::Texture getTexture();
    void setPosition(float x, float y);
    void setSize(int x, int y);
    void setTexture(const std::string &textureButton, int x, int y);
    void createSprite(int x);
    sf::Sprite getSprite();

private:
    sf::Vector2<int> _position;
    sf::Vector2<int> _size;
    sf::Texture _texture;
    sf::Sprite _sprite;
};

#endif // BUTTON_H
