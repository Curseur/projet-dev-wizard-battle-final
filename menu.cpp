#include "menu.h"

Menu::Menu(float width, float height)
{
    if(!font.loadFromFile("./assets/arial.ttf"))
    {

        //handle error
    }

    menu[0].setPosition(110, 580);
    menu[0].setSize(280, 120);
    menu[0].setTexture("./assets/OptionButton2.png", 280, 120);
    menu[0].createSprite(0);

    menu[1].setPosition(500, 580);
    menu[1].setSize(280, 120);
    menu[1].setTexture("./assets/PlayButton2.png", 280, 120);
    menu[1].createSprite(0);

    menu[2].setPosition(890, 580);
    menu[2].setSize(280, 120);
    menu[2].setTexture("./assets/ExitButton2.png", 280, 120);
    menu[2].createSprite(0);

    selectedItemIndex = 0;
}

Menu::~Menu()
{

}


void Menu::draw(sf::RenderWindow &window)
{
    for(int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
    {
        window.draw(menu[i].getSprite());
    }
}

void Menu::MoveLeft()
{
    if(selectedItemIndex - 1 >= 0)
    {
        menu[selectedItemIndex].createSprite(0);
        selectedItemIndex--;
        menu[selectedItemIndex].createSprite(1);
    }
}

void Menu::MoveRight()
{
    if(selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS)
    {
        menu[selectedItemIndex].createSprite(0);
        selectedItemIndex++;
        menu[selectedItemIndex].createSprite(1);
    }
}

Button Menu::getButton(int x)
{
    return menu[x];
}
