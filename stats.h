#ifndef STATS_H
#define STATS_H

#include <string>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

class Stats
{
    int mMin;
    int mMax;
    int mValue;
    sf::Texture mTexture;
    sf::Sprite mSprite;
    sf::Vector2<int> mPos;
    sf::Vector2<int> mSpriteSize;

public:
    Stats(int val = 5,
          int min = 0,
          int max = 10,
          const std::string tex = "",
          sf::Vector2<int> pos = {0, 0},
          sf::Vector2<int> sprSize = {0, 0});
    void UpdateValue(int newVal);
    int getValue();
    sf::Sprite getSprite();

private:
    void UpdateSprite();
    std::string FileName;
    sf::Image* textureImage;
};

#endif // STATS_H
